![TBZ-Logo](x_gitressources/tbz_logo.png)

# Entwickler-Tools (T1)

TBZ Informatik Modul 293 - Webauftritt erstellen und veröffentlichen

[TOC]

## Lernziele

- Ich kann einen geeigneten Editor für HTML/CSS nennen.
- Ich kann eine Entwicklungsumgebung für HTML/CSS einrichten.
- Ich kann hilfreiche Erweiterungen für die Webentwicklung nennen und installieren.
- Ich kann ein Git-Repository für ein Webprojekt einrichten.
- Ich kann eigenen Code ein- und auschecken.
- Ich kann eine Webseite mit den Browser-Tools (Developer Tools) untersuchen.



## Entwicklungsumgebung

Grundsätzlich benötigen sie keine speziellen Programme, um HTML- oder CSS-Code zu schreiben. Es reicht ein einfacher Texteditor wie z.B. Notepad.

Ein Programm welches HTML und CSS unterstützt, hilft jedoch enorm, da sogenannte *Linter* (Algorithmen zum Erkennen von Fehler) integriert sind und auf fehlerhafte Tags und Anweisungen aufmerksam macht. Weiter können die Programme Tags und Anweisungen automatisch schliessen und können bereits während dem Tippen eine Liste von möglichen Tags und Anweisungen vorschlagen. Folgend die wichtigsten Features von HTML-Editoren:

- Autocompletion: Schlägt mögliche Anweisungen und Tags vor während sie tippen oder schliesst automatisch einen Tag.
- Syntax Highlighting: Hebt Inhalte farblich ab, so dass der Text besser lesbar wird.
- Error Detection: Zeigt Fehler in der Syntax an.
- Split-Screen View: Zeigt zwei Ansichten, eine mit dem Code und eine mit dem resultierenden Inhalt.



## Visual Studio Code

Für unseren Unterricht verwenden wir **Visual Studio Code** (VSC), aber sie sind grundsätzlich frei einen anderen *HTML Editor* zu verwenden.

Mit VSC können sie viele Erweiterungen herunterladen, die sie beim Entwicklungsprozess unterstützen, nicht nur für HTML/CSS.

Sie können VSC [hier herunterladen](https://code.visualstudio.com/download).

In VSC kann es ein wenig verwirrend sein, dass man vergeblich *Projekte* sucht. Dies kommt daher, dass man in VSC einen belieben Ordner öffnen kann und dieser dann als Projekt angesehen wird. VSC erkennt den Inhalt und weiss aufgrund dessen, um was für ein Softwareprojekt es sich handelt.

**Achtung**: Verwenden sie die Workspaces nicht - es sind keine Projekte.

![vsc](x_gitressources/visual-studio-code.png)

Installieren sie anschliessend auch eines der beiden Erweiterungen in VSC:

- HTML Preview: Lässt sie im Split-Screen-Modus eine HTML-Seite bearbeiten und sie gleichzeitig als Vorschau anzeigen
- Live Server: Started ein Webserver, der Änderungen direkt reflektiert. Live Server startet aber in einem eigenen Fenster und nicht in Split-Screen

![extension](x_gitressources/vsc-html-preview.png)



## Github, Gitlab

Wir werden unsere Arbeiten und Projekte in Github verwalten. Sie hatten bereits eine Einführung in Git im Modul 231 und daher wird dies hier nicht mehr wiederholt.



## Developer Tools / Browser Tools

Alle Browser werden ausgeliefert mit Tools, die den Webentwicklern helfen Applikationen zu erstellen. Die Tools sehen auf allen Browsern ähnlich aus. In diesem Kapitel wird beispielhaft der Browser Chrome verwendet.

Das **Tastaturkürzel**, um die **Developer Tools (DevTools)** zu öffnen ist immer **F12**. Es öffnet sich ein zusätzliches Fenster - entweder links, unterhalb oder sogar überlappend (den Ort kann man anschliessend einstellen).

Eine weitere Art wie sie direkt die DevTools öffnen ist, indem sie auf der Webseite auf einen beliebigen Ort rechtsklicken und "Inspect" anklicken. Nun werden die DevTools geöffnet und zwar direkt bei dem HTML-Element welches sie mit Rechtsklick ausgewählt haben.

![devTools](x_gitressources/DevTools.png)

Die wichtigsten Elemente der DevTools sind im Bild markiert:

- **Mobile/Desktop**: Sie wechseln die Seite auf die Mobile-Ansicht, sie simulieren also ein Smartphone
- **Elements**: Sie können die gesamte HTML-Struktur sehen. Elemente lassen sich aufklappen. Sie sehen auch die CSS-Anweisungen
- **Console**: Hier können sie Fehler und Warnungen sehen, z.B. wenn JavaScript-Dateien Fehler werfen. Dieser Bereich ist für unser Modul nicht relevant aber für dynamische Applikationen schon.
- **Sources**: Hier sehen sie alle Dateien, die geladen wurden und sie sehen auch von welcher Domain/Quelle die stammen.
- **Network**: Auch hier sehen sie alle Dateien, die geladen wurden, aber nicht kategorisiert nach Quelle, sondern die Detail des Protokolls. Dazu kommt mehr in einem späteren Kapitel.
- **Application**: In diesem Bereich finden sie alles über Cookies und und lokale Speicher. Dieser Bereich ist für unser Modul nicht relevant aber für dynamische Applikationen schon.
  



## Checkpoints

- [ ] Ich kann einen geeigneten Editor für HTML/CSS nennen.
- [ ] Ich kann eine Entwicklungsumgebung für HTML/CSS einrichten.
- [ ] Ich kann hilfreiche Erweiterungen für die Webentwicklung nennen und installieren.
- [ ] Ich kann ein Git-Repository für ein Webprojekt einrichten.
- [ ] Ich kann eigenen Code ein- und auschecken.
- [ ] Ich kann eine Webseite mit den Browser-Tools (Developer Tools) untersuchen.



**Informatik Modul 293 - Webauftritt erstellen und veröffentlichen**

Letzte Aktualisierung: 15.05.2022, Yves Nussle
