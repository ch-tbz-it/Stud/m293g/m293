![TBZ Logo](../x_gitressources/tbz_logo.png)

# Erweitertes CSS (D4)

TBZ Informatik Modul 293 - Webauftritt erstellen und veröffentlichen

[TOC]


## Lernziele
1. Ich kann den Unterschied zwischen transform und transition erklären.
2. Ich kenne den Unterschied zwischen transition und animation und kann erklären wann welches eingesetzt wird.
3. Ich kann CSS-Variablen verwenden und bin mir der limitierten Browserunterstützung bewusst.
4. Ich kann Webfonts in eine Webseite einbinden.
5. Ich kann die Vorteile von Icon-Schriften gegenüber Sprites erklären.
6. Ich weiss was CSS Preprocessoren sind und bin mir der zusätzlichen Komplexität durch die Generierung des CSS-Codes bewusst.


## Einführung
Sie haben bereits alle CSS-Grundlagen in den vorhergehenden Kompetenzen erarbeitet. Bei dieser Kompetenz werden Sie viele weitere Features kennen lernen, die Ihnen CSS bietet. Das Ziel ist es, Ihnen einen Überblick zu geben über die erweiterten Möglichkeiten von CSS. In den [Übungen](Uebungen.md) dürfen Sie auswählen, welche (mindestens drei) der vorgestellten erweiterten Features von CSS Sie ausporbieren / anwenden wollen.

Die Folgenden Themen werden behandelt:
- **CSS-Transform**: Damit werden Elemente transformiert. Elemente können gedreht, verschoben, verzogen oder skaliert werden.
- **CSS-Transition**: Damit wird festgelegt, wie der Übergang von einem CSS-Zustand in einen Anderen ablaufen und wie lange dieser Übergang dauern soll. So kann beispielsweise ein "sanfter" Wechsel von Normalzustand eines Buttons in den Hover-Zustand definiert werden.
- **CSS-Animationen**: Geht noch etwas weiter wie CSS-Transition. Mit `animation` können Start- und Zielzustand vom Aussehen her mit CSS-Anweisungen festgelegt werden (und auch Zustände dazwischen). Anschliessend wird die Animation einem HTML-Element via CSS Selektor zugewiesen. Dieses Element wird anschliessend wie gewünscht animiert.
- **CSS-Variablen**: Damit können redundante CSS-Anweisungen vermieden werden. Besonders sinnvoll ist der Einsatz von Variablen bei Farben oder Schriftarten. Um die Seite umzufärben, muss - wenn Variablen konsequent verwendet werden - nur noch an einer Stelle geändert werden.
- **Webfonts / Icon-Schriftarten**: Es ist möglich via CSS Schriftarten einzubinden und anzuzeigen, die nicht auf dem Rechner des Webseitenbesuchers installiert sein müssen. Dies geschieht dadurch, dass die Schriften in CSS definiert werden. Bei solchen Schriften spricht man von Webfonts. Besteht die Schriftart nur aus Icons, spricht man von Icon-Schrift. Diese wird anstelle von Sprites für die Darstellung von Icons auf der Webseite verwendet.
- **LESS, SASS und SCSS**: Bei den drei handelt es sich um CSS Preprocessoren, die die Funktionalität von CSS um weitere Features erweitern und CSS-Code generieren können. Die CSS Preprocessoren bieten auch die Möglichkeit minified-Versionen der CSS-Files zu generieren. Dadurch wird Bandbreite gespart.

Im Folgenden werden die einzelnen Themen detailierter erläutert.

## CSS-Transform
Mit CSS-Transform werden Elemente transformiert. Transformieren bedeutet drehen (`rotate( ... )`, `rotate3d( ... )`), verschieben (`translate( ... )`, `translate3d( ... )`), verzogen (`skew( ... )`) oder skaliert (`scale( ... )`, `scale3d( ... )`) werden.

Bei den angegebenen Properties handelt es sich bereits um eine verkürzte Schreibweise. Mit `rotateX( ... )`, `rotateY( ... )` und `rotateZ( ... )` können die Werte beispielsweise auch einzel angepasst werden. Ähnliche schreibweisen gibt es auch für die anderen Transformationen ([Details](https://www.w3schools.com/cssref/css3_pr_transform.php)). 

Mit `matrix( ... )` und `matrix3d( ... )` ist es ebenfalls möglich, Elemente zu drehen, zu verschieben, zu skalieren und zu verziehen (alles in einer Anweisung). Die Matrix arbeitet mit einem Gleichungssystem in welchem die Variablen verwendet werden um die X- und Y-Koordinaten für jeden Punkt im Element neu zu berechnen. Der Nachteil ist, dass die Berechnung der nötigen Werte für das gewünschte Resultat relativ schwierig sein kann. Die Berechnung wird bei [selfhtml](https://wiki.selfhtml.org/wiki/CSS/Funktionen/matrix()) schön erklärt.

>:information_source: **_Hinweis:_**
>
>Unter https://www.mediaevent.de/css/transform.html finden Sie eine Anleitung inkl. interaktiver Beispiele, die Ihnen helfen das Thema CSS-Transform zu vertiefen bzw. auszuprobieren. Das Thema CSS-Animationen wird auf dieser Seite ebenfalls angerissen.

## CSS-Transition
Die `transition` Property definiert, wie der Übergang von einem Set an CSS-Regeln zu einem anderen Set an CSS-Regeln vonstatten geht. Zustände wie beispielsweise `:hover` können verwendet werden, um mit `transition` eine "animation" vom Normalzustand in den Hover-Zustand zu definieren.

Bei den Übergängen (transitions) wird definiert, wann die Animation startet (`transition-delay`), wie lange diese dauert (`transition-duration`) und wie animiert werden soll (`transition-timing-function` - linear = gleichmässig, ease-in = lansamer Start, dann schnell... etc.). Die Animation kann auf alle oder auf ausgewählte CSS-Properties (`transition-property: width;` = Animation greift nur für Veränderungen der Breite) angewendet werden. Animantionen können nicht nur auf Dimensionen wie Breite und Höhe, sondern beispielsweise auch auf Farben, was einen "sanften" Farbwechsel zur Folge hat.

Bei [w3schools](https://www.w3schools.com/css/css3_transitions.asp) und [mediaevent](https://www.mediaevent.de/css/transition.html) finden Sie Beispiele und weiterführende Erklärungen zu dem Thema.

## CSS-Animationen
Animationen sind ähnlich wie Transitions, gehen aber noch etwas weiter. Bei Animationen werden auch die Übergänge von einzelnen CSS-Zuständen animiert. Im Gegensatz zu Transitions werden aber bei `animation` einzelne `@keyframes` definiert. Ein Keyframe kann als "Set von CSS-Anweisungen" angesehen werden, dass zu einem bestimmten Zeitpunkt im Laufe der Animation gültig ist. Der Zeitpunkt der Keyframes wird dabei in % (0% = Animationsstart, 100% = Animationsende) oder mit `from` und `to` (bei nur zwei Zuständen) angegeben.

Verwenden Sie die Beschreibung und Beispiele von [w3schools](https://www.w3schools.com/css/css3_animations.asp), um sich im Thema weiter zu vertiefen.

## CSS-Variablen
CSS erlaubt die Verwendung von Variablen. Dieses Feature ist vorallem im Zusammenhang mit der Verwendung von Farben sinnvoll. Es kann aber auch für andere Werte, die an mehreren Orten wiederverwendet werden sollen, eingesetzt werden.

Folgender Beispielcode definiert zwei Variablen mit den Namen `--red` und `--blue`:
~~~css
:root {
    --red: #aa0000;
    --blue: #0000aa;
}
~~~

**Wichtig:** Variabelnamen müssen immer mit -- starten und sind case sensitive (Gross/Klein-Schreibung wird beachtet).

Die Variable wird im CSS-Code wie folgt verwendet:
~~~css
p {
    color: var(--red);
    background-color: var(--blue);
}
~~~

Die Verwendung von Variablen macht dann sinn, wenn die gleichen Werte an mehreren Orten eingesetzt werden sollen. So kann beispielsweise bei den Farben ohne viel zusatzaufwand an einer Stelle die Variable angepasst werden und schon ist die Seite umgefärbt.

>:information_source: **_Hinweis:_**
>
>Dieses Feature wurde erst im Laufe der Zeit durch die Browserhersteller unterstützt. Beachten Sie deshalb, dass die Unterstützung durch die Browser noch nicht flächendeckend gewährleistet ist. Mit derzeit > 95% Unterstützung (Quelle: https://caniuse.com/css-variables) ist das Feature in der Zwischenzeit aber auf einem Niveau angelangt wo es gut eingesetzt werden kann.

Verwenden Sie die Beschreibung und Beispiele von [w3schools](https://www.w3schools.com/css/css3_variables.asp), um sich im Thema weiter zu vertiefen.

## Webfonts / Icon-Schriftarten
Damit auf Webseiten Schriftarten eingesetzt werden können, müssen diese (ohne die Verwendung von Webfonts) auf dem Computer des Clients zwingend installiert sein. Bei Webfonts wird das Problem umgangen indem der Webserver die Schriftdatei via CSS verlinkt und somit dem Browser zum Download anbietet. Bei Webfonts werden die Schriften nicht dauerhaft im System des Clients installiert, sondern nur für die Darstellung der Schrift auf der entsprechenden Webseite eingesetzt.

>:information_source: **_Hinweis:_**
>
>Bitte beachten Sie die Lizenzbedingungen von den Schriftarten die Sie einsetzen möchten. Viele gekaufte Schriften könnten zwar als Webfont eingebunden werden, dürfen es aber nicht aufgrund der Lizenzbestimmungen des Herstellers der Schriftart. Bei Unsicherheit sollten Sie sich deshalb immer mit dem Hersteller in Verbindung setzen um zu klären ob der Einsatz als Webfont erlaubt ist oder nicht.
>
>Beachten Sie weiter: nicht ganz alle Browser unterstützen dieselben Formate von Schriftdateien. Je nach dem müssen Schriftdateien allenfalls erst in ein anderes Format konvertiert werden oder mehrer Formate via CSS verlinkt werden. Unter [developer.mozilla.org](https://developer.mozilla.org/en-US/docs/Learn/CSS/Styling_text/Web_fonts) finden Sie weitere Informationen zum Thema Webfont (inkl. Tipps wo und wie die Fonts konvertiert werden können).

Mit der `@font-face`-Regel werden die Font-Files im `src`-Attribut verlinkt. Mit dem Attribut `font-family` innerhalb der `@font-face`-Regel wird der Name der Schrift definiert (dieser darf beliebig sein). Mit `font-family` innerhalb der normalen CSS-Anweisung wird die Schriftart anschliessend verwendet:

~~~css
@font-face {
  font-family: myFirstFont;
  src: url(sansation_light.woff);
}

div {
  font-family: myFirstFont;
}
~~~
Quelle Beispiel: https://www.w3schools.com/css/css3_fonts.asp

Eine Icon-Schrift ist eine Schrift, die als Schriftzeichen keine Buchstaben, sondern Bilder (Icons) besitzt. Die Idee ist, dass die Interaktiven Elemente einer Webseite (beispielsweise Warenkorbsymbol, Bearbeiten- oder Löschen-Buttons) nicht einzelne Bilder sind, sondern die Zeichen von einer Schriftart sind. Damit muss nur eine Schrift geladen werden um alle Icons darzustellen die die Seite braucht. Alternativ wurde früher mit Sprites (ein grosses Bild mit allen Icons drauf) gearbeitet. Das Problem bei Sprites war jedoch, dass die Bilder in einer fixen Farbe vorlagen und diese nicht einfach geändert werden konnte. Bei Icon-Schriften definiert die Schriftfarbe die Farbe des Icons. Ausserdem sind die Icons vektorbasiert. D. h. die Icons können beliebig skaliert werden ohne dass die Ränder verpixelt dargestellt werden.

Eine bekannte Icon-Schrift die sowohl in einer frei verfügbaren wie auch einer PRO-Version zur Verfügung steht ist https://fontawesome.com

## LESS, SASS und SCSS
Bei LESS und SASS handelt es sich um zwei unterschiedliche CSS-Preprocessoren. SCSS ist eine spezielle Syntax-Form vom SASS Preprocessor ([Details](https://www.ionos.de/digitalguide/websites/web-entwicklung/sass/)). CSS Preprocessoren wurden geschaffen, weil die Entwickler einige Features (Baumstruktur, Verwendung von Includes, Variabeln, Minifying etc.) vermissten. Einige der vermissten Features wurden in den Vergangenen Jahren nun ins CSS mit übernommen, weshalb die CSS Preprocessoren etwas an Bedeutung verlohren haben. Fakt ist aber, dass diese bei grösseren Frameworks und Webseiten eingesetzt werden und Sie wissen sollten, dass es diese gibt und wie die Preprocessoren grundsätzlich arbeiten.

Wichtig ist festzuhalten: 
- CSS Preprocessoren generieren immer normalen CSS-Code. 
- In der Regel wird der Code - vorallem wenn Verschachtelung verwendet wird - grösser wie wenn der gleiche Code manuell erstellt wurde.
- CSS Preprocessoren ermöglichen es, den generierten Code gleich zu "minifyen" was den Nachteil des etwas längeren Codes wieder etwas relativiert.

Um LESS oder SASS einsetzen zu können, wird eine zusätzliche Software für den Generiervorgang benötigt. Das System bzw. setup der Entwicklungsumgebung wird dadurch etwas komplexter. Die grösseren IDE's bieten integrationen für LESS und SASS an. Eine Liste von möglichen Standalone-Softwarepaketen wurde von [uideck.com](https://uideck.com/blog/free-sass-less-compilers-css-preprocessors/) zusammengestellt.


## Checkpoints
- [ ] Ich kann den Unterschied zwischen transform und transition erklären.
- [ ] Ich kenne den Unterschied zwischen transition und animation und kann erklären wann welches eingesetzt wird.
- [ ] Ich kann CSS-Variablen verwenden und bin mir der limitierten Browserunterstützung bewusst.
- [ ] Ich kann Webfonts in eine Webseite einbinden.
- [ ] Ich kann die Vorteile von Icon-Schriften gegenüber Sprites erklären.
- [ ] Ich weiss was CSS Preprocessoren sind und bin mir der zusätzlichen Komplexität durch die Generierung des CSS-Codes bewusst.



**Informatik Modul 293 - Webauftritt erstellen und veröffentlichen**

Letzte Aktualisierung: 14.05.2022, Florian Huber