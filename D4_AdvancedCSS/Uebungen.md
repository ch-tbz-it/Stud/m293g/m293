# D4 Übungen

> Machen Sie die Übungen ohne Boilerplate, Framework oder ähnliches. Schreiben Sie 100% eigenen Code und präsentieren Sie das Ergebnis Ihrer Lehrperson. Geben Sie das Ergebnis als ZIP-Datei ab.

In der Theorie haben Sie die folgenden Features von CSS kennengelernt:
- CSS-Transform
- CSS-Transition
- CSS-Animationen
- CSS-Variablen
- Webfonts / Icon-Schriftarten
- LESS, SASS und SCSS

Wählen Sie aus diesen sechs Themen mindestens drei aus. Wählen Sie die Features, die Sie besonders interessieren. Bauen Sie nun eine Webseite (eine einzelne Seite genügt - der CSS-Code darf im `<style>`-Tag im header der Seite platziert werden - oder auch als eigenes CSS-File... was Ihnen lieber ist). Stellen Sie sicher, dass Sie in Ihrer Webseite die ausgewählten Features verwenden bzw. die ausgewählten Tools für die Erzeugung des CSS-Codes einsetzen.

Erklären Sie wie Ihre Webseite aufgebaut ist und Ihr Code funktioniert.
