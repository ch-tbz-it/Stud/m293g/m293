# m293 Webauftritt erstellen und veröffentlichen

1.Lehrjahr: Q3 / Q4

Modulbeschreibung: https://www.modulbaukasten.ch/module/293/1/de-DE?title=Webauftritt-erstellen-und-ver%C3%B6ffentlichen


*Problem*
<br>Applikationsentwickler, und verschärft solche, die im Basislehrjahr sind, kennen die Modulinhalte zum grossen Teil schon. Die eigentlichen Modulinhalte sind für solche Lernende schon rasch erreicht und müssen/werden so in erweiterten, bzw. in vertieften Kenntnissen geschult.


# Themen und Unterlagen

## Entwicklungsumgebung, Tools und HTTP

## HTML und Formulare
Auch wenn in den folgenden Seiten vieles zusammengefasst ist, existieren verschiedenen Online Tutorials, welche vieles differenzierter erklärt und weitere Übungsmöglichkeiten zulässt. Folgend eine Auswahl an möglichen Optionen:
- [https://www.w3schools.com/html/](https://www.w3schools.com/html/)
- [https://www.tutorialspoint.com/html/index.htm](https://www.tutorialspoint.com/html/index.htm)
- [https://html.com/](https://html.com/)
- [Bücher](https://www.programming-book.com/beginning-html-xhtml-css-and-javascript/)

## Design und Darstellung
