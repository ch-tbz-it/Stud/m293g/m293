![TBZ-Logo](../x_gitressources/tbz_logo.png)

# Web-Optimierung und -Automatisierung (T4)

TBZ Informatik Modul 293 - Webauftritt erstellen und veröffentlichen

[TOC]

## Lernziele

- Ich kann erklären was Node.js ist und wozu ein Package Manager dient.
- Ich kann einen Package Manager anwenden und Code-Bibliotheken herunterladen.
- Ich kann erklären wieso Bundling und Minifying sinnvoll ist.
- Ich kann npm Pakete installieren und ausführen.
- Ich kann erklären wie man Automatisierungen sinnvoll einsetzt.
- Ich kann Tools anwenden, welche die Dateien für eine Webseite optimieren.



## Übersicht

Zusammen mit der Entwicklung von HTML 5 und dem Begriff von Web 2.0, wurden verschiedene Technologien und JavaScript Frameworks entwickelt, die die Webprogrammierung vereinfacht. In diesem Kapitel behandeln wir drei Technologien, die stark verbreitet sind:

- Package Manager
- Optimierer / Bundler / Minifier
- Automatisierung / Task Runner



## Node.js

Node.js ist eine open-source, cross-platform Umgebung, die JavaScript ausserhalb von Browsern ausführen kann. Mit node.js kann einfach ein Webserver gestartet werden und JavaScript server-seitig aufgerufen werden. Wir werden nicht weiter auf Node.js eingehen, aber in den folgenden Kapiteln auf zwei Tools, die zusammen mit Node.js installiert werden (Package Manager und Task Runner).

[Laden sie Node.js hier herunterladen](https://nodejs.org/en/download/).

Weitere Informationen, falls sie sich für Node.js interessieren, [finden sie hier](https://nodejs.org/en/docs/guides/getting-started-guide/).



## Package Manager

Ein Package Manager (PM) verwaltet Pakete, die sie aus dem Internet herunterladen. Pakete haben eine Version und Abhängigkeiten zu anderen Paketen, die der PM automatisch zusätzlich herunterlädt. Stellen sie sich die Pakete als Bibliotheken vor, die ihnen die Programmierung vereinfacht - in diesem Fall JavaScript Programmierung und server-seitige Pakete, die sie bei der Optimierung und Publikation von Code unterstützt.

![PM-Scheme](x_gitressources/PackageManager.png)

Bekannte Package Manager für verschiedenen Sprachen sind zum Beispiel:

- npm und yarn: JavaScript
- Maven: Java
- NuGet: C#
- PyPM: Python

### npm: Node Package Manager

In diesem Modul verwenden wir [*npm*](https://www.npmjs.com/), weil dieser weit verbreitet ist. [*yarn*](https://yarnpkg.com/) ein weiterer gleichwertiger PM. Interessanterweise wird yarn via npm installiert. Sie installieren also ein PM via einem PM.

[Unter diesem Link finden sie eine vollständige Doku zu npm.](https://docs.npmjs.com/)

*npm* ist ein CLI (Command Line Interface), welches sie z.B. im Command Prompt von Windows aufrufen. *npm* benötigt die folgenden Objekte im Root ihrer Web-Applikation:

- Datei *package.json*: Diese Datei enthält die Projekt-Informationen und die Pakete/Module, die sie installieren möchten.
- Datei *package-lock.json*. Diese Datei enthält den kompletten Baum der Abhängigkeiten, aufgrund der Datei package.json. Es ist möglich, dass ein Paket zusätzliche Abhängigkeiten hat, welche in dieser Datei aufgeführt werden. Die Datei kann jederzeit gelöscht und neu (automatisch) generiert werden.
- Ordner *node_modules*. In diesem Ordner werden alle heruntergeladenen Pakete abgelegt. Sie können den Ordner jederzeit löschen und (automatisch) neu generieren lassen.

**Bei den Übungen werden sie Schrittweise durch den Prozess geführt.**

### npx: Package Runner

Wie bereits erklärt gibt es Pakete, die für die Web Applikation benötigt werden und Pakete, die server-seitig ausgeführt werden (z.B. webpack). Um server-seitige Pakete auszuführen, muss man im entsprechenden *bin*-Verzeichnis die Datei aufrufen (./node_modules/.bin/your-package) oder sie fügen das Script in package.json ein (dazu später mehr).

Um dies zu vereinfachen wurde das Tool *npx* hinzugefügt, welches die korrekte Datei aufruft (`npx your-package`). Wir werden dies im folgenden Kapitel benötigen und demonstrieren. [Die Dokumentation zu *npx* finden sie unter diesem Link](https://docs.npmjs.com/cli/v8/commands/npx)

Also kurz zusammengefasst: Während *npm* Pakete herunterlädt, führt *npx* Pakete aus.

## Optimierung durch Bundling und Minifying

Wenn sie viele JavaScript und/oder CSS Dateien haben, wird der Browser für jede Datei einen eigenen Request starten. Die Webseite wird erst korrekt dargestellt, wenn alle Dateien geladen wurden. Um die Wartezeit zu verkürzen und zu optimieren, wurden die beiden Konzepte *Bundling* und *Minifying* (oder Compressing, Optimizing) eingeführt.

**Bundling**: Es werden alle JavaScript (oder CSS) Dateien zusammengefasst in eine Datei, so dass nur wenige Dateien vom Browser angefragt werden.

**Minifying**: Die Dateien werden minimiert. Speziell bei JavaScript-Dateien ist dies möglich, indem alle Variablen und Funktionsnamen gekürzt werden. Zusätzliche werden alle Zeilenumbrüche und unnötigen Leerschläge entfernt. Die zuletzt genannten Optimierungen sind auch für CSS-Dateien möglich.

Bekannte Tools: [Webpack](https://webpack.js.org/), [Browserify](https://browserify.org/)

### Webpack (kurz abgeschweift...)

Webpack ist sehr umfangreich und eignet sich sehr gut für SPAs (Single Page Applications) in denen alle CSS und JavaScript Inhalte zusammengefasst und in einen einzelnen Einstiegspunkt (index.html) eingefügt werden. Die Anzahl Plugins ist riesig und die Komplexität hoch. 

Für unser Modul eignet sich Webpack nicht besonders, da wir mit mehrere statischen Seiten arbeiten. es lohnt sich aber auf jeden Fall sich selbstständig in die [Konzepte von Webpack einzulesen](https://webpack.js.org/concepts/).

### Minifier

Ausser dem bekannten Webpack gibt es verschiedene Minifier, die man verwenden kann. In diesem Modul werden wir die folgenden beiden verwenden:

- [html-minifier](https://www.npmjs.com/package/html-minifier): Kann alle HTML-Dateien eines Ordners minimieren.
- [css-minifier](https://www.npmjs.com/package/css-minify): Kann alle CSS-Dateien eines Ordners minimieren.

#### html-minifier

Installiert wird dieses Tool wieder mit npm `npm install html-minifier`.  Ausgeführt wir das Packet dann via `npx` und nicht `npm` , z.B. `npx html-minifier --input-dir ./src --output-dir ./dist --collapse-whitespace --file-ext html`

Der genannte Befehl minimiert alle Dateien im Order "src" mit der Endung .html und kopiert die komprimierten Versionen in den order "dist".

#### css-minify

Auch dieses Tool wird wieder mit npm installiert `npm install css-minify`.  Ausgeführt wir das Packet dann via `npx` und nicht `npm` , z.B. `npx css-minify -d ./src -o ./dist && move ./dist/styles.min.css ./dist/styles.css`

Der genannte Befehl minimiert alle Dateien im Order "src" mit der Endung .css und kopiert die komprimierten Versionen in den order "dist". Wenn man in der Dokumentation nachliest, erfährt man. dass ein "min" hinzugefügt wird. Aus "styles.css" wird also "styles.min.css". 

Unsere HTML-Dateien referenzieren allerdings styles.css. wir werden dies später noch lösen.

## Automatisierung / Task Runner

Wir haben bisher Packete via `npx` einzeln aufgerufen . In einer Entwicklungsumgebung gibt es aber weitere Tasks, die normalerweise anfallen und wir wollen nicht alle Befehle einzeln aufrufen. Oft möchten sie z.B. das Zielverzeichnis zuerst aufräumen, resp. den Inhalt löschen und Konfigurationsdateien hinzufügen, die spezifisch für eine Umgebung erstellt wurden (Produktion, Test, Entwicklung). Abhängig davon für welche Umgebung sie automatisieren, müssen andere Config-Dateien hinzugefügt werden. **Hinweis**: Halten sie Konfigurationsdateien mit Passwörter **nie** in Github/Gitlab. 

Mit einem Task Runner können sie verschiedene automatisieren und hintereinander ausführen lassen. Die folgenden Task Runner sind häufig in gebrauch: [Node Scripts](https://docs.npmjs.com/cli/v8/using-npm/scripts), [Grunt](https://gruntjs.com/), [Gulp](https://gulpjs.com/). Ein Task Runner macht eigentlich nicht viel mehr aus die Befehle in der Konsole auszuführen. Sie können also auch normale CMD- oder Bash-Befehle übergeben und die werden im Task Runner ausgeführt. Genauso gut können sie aber auch selbst ein Bash-Skript schreiben und könnten so auf den Task Runner verzichten.

Wir verwenden im folgenden *Node Scripts*. Die verschiedenen Technologien sind gleichwertig und *Node Scripts* wird direkt mit Node.js ausgeliefert. Wenn sie ihr bestehendes package.json anschauen, finden sie die Sektion *scripts*. Hier können sie ihre Scripts einfügen, wobei bereits ein script mit dem Namen *test* hinzugefügt wurde. Standardmässig wird nur eine Ausgabe mit *echo* erstellt.

~~~~json
    "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1"
    },
~~~~

 Rufen sie den Befehl `npm run test` auf und testen sie was geschieht:

1. Sie sehen die Ausgabe "Error: no test specified"
2. Eine Fehlermeldung erscheint, da im Script *exit 1* aufgerufen wird. *Exit 0* würde keinen Fehler werfen
3. Die &&-Zeichen verarbeiten die beiden Befehle nacheinander. 

**Bei den Übungen werden wir weitere Scripts hinzufügen und Beispiele durcharbeiten**



**Informatik Modul 293 - Webauftritt erstellen und veröffentlichen**

Letzte Aktualisierung: 22.07.2022, Yves Nussle
