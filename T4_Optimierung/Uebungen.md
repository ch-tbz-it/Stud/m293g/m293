# T4 Übungen



### Übung 1 - npm verwenden (npm)

1. Erstellen sie ein neues Verzeichnis mit einer **leeren** index.html Datei
2. Rufen sie ihr Command Prompt auf.
3. Wechseln sie in das neu erstellte Verzeichnis. **Befehl**: `cd <Ihr-Verzeichnis>`
4. Initialisieren sie ihre Seite. **Befehl**: `npm init`. Sie werden durch ein Menu geführt und angewiesen ein paar Felder auszufüllen. Sie können überall einfach die *Eingabe* drücken.
5. Installieren sie ein Paket, z.B. *lodash*. lodash ist eine Bibliothek von JavaScript-Funktionen die ihnen die Programmierung vereinfacht, z.B. Schleifen durch Objekt-Listen. **Befehl**: `npm install lodash`

Überprüfen sie nun, ob die drei Objekte in ihrem root-Verzeichnis angelegt wurden (package.json, package-lock.json, node_modules).

Finden sie - [mit Hilfe der Doku](https://docs.npmjs.com/) - heraus was der Unterschied wäre, wenn sie `npm install lodash -g` aufgerufen hätten. Testen sie das Verhalten mit einem anderen Paket, z.B. *underscore*.



### Übung 2 - Applikation neu generieren (npm)

Löschen sie die beiden Objekte *package-lock.json* und *node_modules* aus der Übung 1. Rufen sie nun den Befehl `npm install` auf.

Dokumentieren sie was geschieht.



### Übung 3 - Minimierung von html-Dateien (npx, html-minifier)

[Laden sie die Übungsdateien herunter](Uebung03-Sources.zip) und entpacken sie sie in einen Ordner. Die Ordnerstruktur ist ein wenig anders als bisher. Es gibt im root-Verzeichnis zwei Ordner *src* (für Source oder Quelldateien) und *dist* (für Distribution also Endprodukt). Im Ordner *src* finden sie drei Dateien. 

**Schritte**:

1. Initialisieren sie das npm Projekt
2. Laden sie das Paket *html-minifier* herunter, resp. installieren sie es
4. Führen sie den Befehl `npx html-minifier --input-dir ./src --output-dir ./dist --collapse-whitespace --file-ext html` aus.
5. Schauen sie sich nun die Datei im *dist*-Ordner an. Wurde sie komprimiert?



### Übung 4 - Minimierung von CSS-Dateien (npx, css-minify)

In Übung 4 haben sie HTML optimiert. Nun werden wir auch die CSS-Dateien optimieren.

Schritte:

1. Verwenden sie ihre Dateien aus der Übung 3 oder [Laden die Quelldateien neu herunter](Uebung03-Sources.zip). 
2. Sie benötigen die folgenden zusätzlichen npm Pakete: *css-minify* 
3. Führen sie das Packet via `npx` aus.
4. Schauen sie sich die Datei im *dist*-Ordner an. Wurde sie komprimiert?
5. Wenn sie ihre HTML-Datei aus dem *dist*-Ordner aufrufen, sieht sie korrekt aus? Wieso nicht? Wir lösen dieses Problem in Übung 6.



### Übung 5 - Build-Script hinzufügen

Verwenden sie ihre Dateien aus der Übung 4 oder [Laden die Quelldateien neu herunter](Uebung03-Sources.zip).

Erstellen sie einen neuen Script-Eintrag in package.json mit dem Namen "build" und fügen sie die beiden Befehle ein. Sie können zwei Befehle hintereinander ausführen, wenn sie sie mit " && " verknüpfen.

Testen sie ihr Script mit dem entsprechenden Befehl.



### Übung 6 - Build-Script erweitern I

Wir haben gelernt, dass sie in "Node Scripts" einfach CMD- oder Bash-Befehle ausführen können. Dadurch lässt sich das Problem aus Übung 4 vermeiden. Wie gehen sie es an?



### Übung 7 - Build-Script erweitern II

Nun möchten wir das Build Script erweitern. Fügen sie ein neues Script ein mit dem Namen "prebuild". Da sie nun der gleiche Name verwenden wie vorher, einfach mit "pre" am Anfang, wird dieses Script automatisch **zuerst** ausgeführt, wenn sie das Script für "build" aufrufen. [Lesen sie hier die Details nach](https://docs.npmjs.com/cli/v8/using-npm/scripts). 

In diesem Script möchten wir zuerst die Inhalte des *dist*-Verzeichnis löschen. Wir räumen also zuerst auf. Verwenden sie dazu das *npm* Paket [*rimraf*](https://www.npmjs.com/package/rimraf).

Testen sie ihr build-Script und testen sie, ob beide Teile ausgeführt werden.