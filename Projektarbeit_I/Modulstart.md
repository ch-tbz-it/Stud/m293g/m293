![TBZ Logo](../x_gitressources/tbz_logo.png)

# Modulstart

Bei diesem Schritt geht es um das Setup und einige Definitionen, so dass die Arbeit der Lehrpersonen für die Bewertung vereinfacht wird.  

#### a) Gitlab oder Github

Erstellen sie als Gruppe ein Repository für ihre Gruppenarbeit. Beide Mitglieder müssen auf das Projekt Zugriff haben und bearbeiten können. 

Die Lehrperson teilt ihnen mit wo sie den Link auf ihr Repo ablegen können. Grundsätzlich arbeiten wir an der TBZ mit Gitlab. Die Lehrperson lässt sie wissen, ob Github auch möglich ist.

#### b) Ordnerstruktur

Erstellen sie die folgende Ordnerstruktur **und halten sie diese ein**. Es hilft der Lehrperson, die korrekten Dateien zu finden:

Repo-Root

- Uebungen[Ordner]
  - D1[Ordner] (Lösungen für die Übungen aus D1)
  - H1[Ordner] (Lösungen für die Übungen aus H1)
  - ....
- Projekt[Ordner]
  - Grundaufbau[Ordner] (Inhalte ihres Projekts aus Teilprojekt "Grundaufbau")
    - Readme.md
    - Styleguide.xxx (md, docx, etc)
    - Site[Ordner] (hier kommt ihre Website)
    - Templates[Ordner] (hier kommen ihre Templates)
  - Formulare[Ordner] (Inhalte ihres Projekts aus Teilprojekt "Formulare")
    - gleiche Struktur wie bei Grundaufbau...
  - Responsive[Ordner] (Inhalte ihres Projekts aus Teilprojekt "Responsive")
    - gleiche Struktur wie bei Grundaufbau...
  - Optimierung[Ordner] (Inhalte ihres Projekts aus Teilprojekt "Optimierung")
    - gleiche Struktur wie bei Grundaufbau...
  - Medien[Ordner] (Inhalte ihres Projekts aus Teilprojekt "Medien")
    - gleiche Struktur wie bei Grundaufbau...
  - Preprocessor[Ordner] (Inhalte ihres Projekts aus Teilprojekt "Preprocessor")
    - gleiche Struktur wie bei Grundaufbau...

**Achtung**: Kopieren sie die Inhalte aus dem vorherigen Projektschritt jeweils zuerst, so dass sie ihr Resultat nicht überschreiben. Dies widerspricht zwar wie man mit Git arbeiten sollte, aber der Vergleich der Inhalte ist für die Lehrperson einfacher. 

---

&copy;TBZ, 2022, Modul: m293
