![TBZ Logo](../x_gitressources/tbz_logo.png)

# Formulare

Falls sie bei den einzelnen Punkten fragen haben, gehen sie direkt zu der Lehrperson. Sie kann ihnen Beispiele zeigen und eine vertiefte Beschreibung liefern. 

**Kopieren sie ihre Webseite aus Teilprojekt 'Grundaufbau' in den Ordner 'Formulare' bevor sie Änderungen beginnen**

**Publizieren sie ihre Webseite bevor Sie sie abgeben.**

#### a) Registrierungsformular

Erstellen sie ein Registrierungsformular für ihre Webseite. Erweitern sie - falls notwendig - ihre Styles für Formulare und halten sie diese wieder allgemein, so dass sie für weitere Formulare verwendet werden können.

Notwendige Felder:

- Geschlecht (**Radio**)
- Vorname
- Nachname
- E-Mail
- Password
- Adresse
- PLZ
- Ort
- Land (**Select**)
- Datenschutz akzeptieren (**Checkbox**)
- Andere Felder, die für ihre Applikation sinnvoll sind

Weitere Hinweise:

- Die Formularfelder sollen alle Pflichtfelder sein!
- Erstellen sie ihre Styles so, dass sie auch für weitere Formulare anwendbar sind (allgemein halten)
- **Wichtig**: Ihre Formlabels und Formfelder sollen **nebeneinander** stehen, nicht untereinander.

Da ihr Projekt einen klickbaren Prototypen darstellt, müssen sie auch eine entsprechende Bestätigungsseite erstellen. Wenn das Formular abgeschickt wird, soll der Anwender auf eine Bestätigungsseite geleitet werden.

![register](x_gitressources/H3_Registrierung.png)

#### b) Login-Formular

Erstellen sie ein Login-Formular für ihre Webseite. Nach dem Login können sie direkt auf den eigentlichen Inhalt weiterleiten. Passen sie das Ziel des Formulars entsprechend an.

Betten sie ihren Login-Link (oder direkt das Formular) in ihrer Webseite ein.

Verwenden sie die Styles aus Punkt a)

#### c) Passwort vergessen-Formular

Normalerweise findet man einen "Passwort vergessen" Link auf Webseiten. Gehen sie auf ihnen bekannte Webseiten und schauen sie nach wie das Formular aussieht. Implementieren sie nun ein solches auch bei ihnen, unter Verwendung der allgemeinen Styles.

Weitere Hinweise

- Auch diese Seite benötigt eine Bestätigungsseite wie die Register-Seite
- Betten sie ihren Login-Link in ihrer Webseite ein.
- Verwenden sie wieder die Styles aus Punkt a)

---

&copy;TBZ, 2022, Modul: m293