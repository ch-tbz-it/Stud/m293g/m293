![TBZ Logo](../x_gitressources/tbz_logo.png)

# Grundaufbau

Falls sie bei den einzelnen Punkten fragen haben, gehen sie direkt zu der Lehrperson. Sie kann ihnen Beispiele zeigen und eine vertiefte Beschreibung liefern. 

#### a) Projektdefinition

Als Gruppe überlegen sie sich ein Projekt, welches sie als HTML-Seite umsetzen möchte. Wenn sie selbst keine Idee haben, kann ihnen die Lehrperson Vorschläge machen. 

In diesem Projekt sind zwei Typen von Inhaltsseiten notwendig. 

- Ein Typ für Lauftext (z.B. Projekt-Intro, About Us, Formulare)
- Ein Typ zum Darstellen von Inhalten, die mehr Platz in der Breite benötigen als ihre erste Seite (z.B. Verkaufsartikel, Musik/Song-Listen, Social Media Inhalte, Sport-Teams, etc). Die Inhalte sollen als Raster dargestellt werden.

In Gitlab wird ein Readme.md im Ordner "Grundaufbau" erstellt. Ihr Projekt muss in 2-5 Sätzen beschrieben werden, so dass ein Leser weiss was ihn erwartet. 

**Abgabe**: Lassen sie ihr Projekt von der Lehrperson bestätigen **bevor** sie weitergehen!

#### b) Wireframes

Erstellen sie nun Wireframes von ihren beiden Inhaltstypen. Es muss klar werden wo sich folgende Elemente befinden: Header, Footer, Inhalt (in welcher Breite mit Platzhalter), Navigation, Logo.

Sie können [draw.io](https://draw.io) oder andere Tools verwenden. draw.io hat bereits Vorlagen für Wireframes. Sie können ihre Wireframes auch zuerst auf Papier zeichnen, falls sie einiges ausprobieren möchten. Übertragen sie es anschliessend in die Software.

![wireframes](./x_gitressources/wireframes.png)

**Fügen sie die Wireframes als Screenshots in ihr Readme.md ein, so dass eine Dokumentation entsteht.**  Natürlich sollen sie auch die Quelldateien im Repo  im Ordner "Templates" ablegen.

#### c) Styleguide

Erstellen sie einen Styleguide für ihre Webseite. Der Styleguide ist **kein CSS**, sondern eine zusätzliche Datei, z.B. im Markdown- oder Word-Format. Erstellen sie diese Datei und fügen sie **eine Referenz** in ihrer Dokumentation auf diese Datei hinzu (so dass ihre Dokumentation erweitert wird).

Folgende Elemente sind **zwingend** notwendig: Logo, Typographie und Farben, erweiterte Wireframes mit den wichtigsten Abständen.

Fügen sie Bilder hinzu, die sich wiederholen (z.B. Aufzählungspunkte, die ausserhalb der Norm sind), **mindestens** aber das Logo. Es gibt auch Webseiten, die ihnen ein Logo generieren, z. B. https://freelogocreator.com/.

Arbeiten sie mit Farbschemen, die sie immer wieder anwenden. Es gibt Webseiten, die ihnen diese Arbeit abnehmen, z. B. https://coolors.co/.

#### d) Template-Seiten

Nachdem nun klar ist wie die beiden Inhaltsseiten aussehen sollen, bilden sie die beiden Wireframes in HTML ab, **ohne konkreten Inhalt zu erstellen**. Platzieren sie die Container an den korrekten Orten. Verwenden sie dabei **eines** der drei Frameworks: Grid, Flexbox, Positional Layout. Mit allen drei können sie die Header, Footer, etc korrekt platzieren. Sie dürfen auch mischen, falls dies notwendig sein sollte. 

Verwendenden sie für die Rasteransicht ein **anderes** Framework (Grid, Flexbox, Positional Layout, flow Layout) als sie für ihre Struktur (Header, Footer, etc) verwendet haben.

Legen sie die Template-Seiten im Ordner "Projekt1/Templates" ab. 

#### e) Inhaltsseiten

Erstellen sie nun die Inhaltsseiten. Dafür kopieren sie die Templates in den Ordner "Site" und erweitern nur noch die Inhalte. Erstellen sie folgende Inhaltsseiten:

- Projekt-Einstiegsseite: Beschreiben sie kurz den Inhalt. Verwenden sie ein Bild als Unterstützung. Lassen sie den Text um das Bild fliessen. Sie können Bilder von der Plattform unsplash.com oder andere freien Webseiten verwenden. Verwenden sie **das erste Template.**
- Eine About-Us Seite: Beschreiben sie sich kurz mit einer Liste von Eigenschaften (z.B. Name Hobby, Arbeits- und/oder Schulort, etc). Verwenden sie die korrekten Listen-Tags. Verwenden sie auch hier wieder Bilder von ihnen oder Avataren, die sie darstellen sollen. Schauen sie auf die Darstellung und überlegen sie sich wie sie sich darstellen möchten. Beispiele finden sie im Internet viele. Verwenden sie **das erste Template.**
- Erstellen sie eine dritte Seite auf der sie ihre Teams, Produkte, etc darstellen. Verwenden sie hier **das zweite Template.**

Verwenden sie jeweils Hx-Tags (h1, h2, etc) für Titel und Untertitel und Paragraphen für Texte.

#### f) Publikation

Publizieren sie die Inhalte auf einer FTP-Seite. Von der Lehrperson haben sie einen Zugang auf einen FTP bekommen. Verwenden sie diesen Zugang und publizieren sie die Seite dort. Via HTTP sollte die Seite nun verfügbar sein. 

---

&copy;TBZ, 2022, Modul: m293