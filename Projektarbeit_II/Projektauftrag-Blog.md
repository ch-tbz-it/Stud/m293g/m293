# Projektauftrag 1



## Blog-Webseite erstellen

Erstellen Sie einen Blog nach Ihrer Wahl. Präsentieren Sie auf der **Startseite** die beliebtesten oder neusten Artikel und bieten Sie eine Übersicht aller Themen an. Besucherinnen und Besucher sollen sich zudem für einen Newsletter eintragen können, damit sie über neue Artikel informiert bleiben.

 Präsentieren Sie auf der **Themenseite** alle Artikel in einer Übersicht und zeigen Sie auf einer Detailseite den jeweiligen Artikel mit Text, Bilder und evtl. Video.  Zu jedem Artikel soll auch die Autorin oder der Autor mit Foto genannt werden. Teilen Sie die Artikel verschiedenen Themen zu. Es sollen alle Artikel oder nur die Artikel eines bestimmten Themas angezeigt werden können.

Bieten Sie auf einer **Kontaktseite** Informationen über die Autoren an und ein Kontaktformular für alle Fragen rund um den Blog. Über ein Dropdown im Formular sollen verschiedene Betreffzeilen ausgewählt werden können, damit das E-Mail-Programm die Fragen später automatisch zuordnen kann.

### Anforderungen

-   100% eigener HTML- und CSS-Code (keine Frameworks)

-   Seitenstruktur mit Header, Main und Footer

-   Grid-Layout für Artikelübersicht und Inhalte

-   Formulare für Newsletter und Kontakt (PHP-Mailer)

-   Responsives Design für Mobile und Desktop

### Umfang

-   Webauftritt mit mindestens 3 Hauptseiten (siehe Sitemap)

-   10 Artikel oder mehr inkl. Text, Bilder, Videos und Autor

### Ziele und Meilensteine

-   Entwurf als Wireframes für alle Seiten gemäss Sitemap

-   Erstellung einer Styleguide-Seite mit Typografie und Farben

-   Umsetzung von Struktur und Layout als Klickprototyp

-   Erfassung aller geforderten Inhalte

-   Finalisierung und Test

-   Veröffentlichung und Präsentation auf einem Webserver

### Zeit

20 Lektionen

### Optional

-   Kommentarfunktion mit Disqus (JavaScript erlaubt)

-   Likes für Artikel (Web Storage API)

### Sitemap

![Sitemap](Assets/b1b364e9558f5e19ecd00e2cd1810040.png)
