# Projektauftrag 2



## Portfolio-Webseite erstellen

Erstellen Sie ein Portfolio nach Ihrer Wahl. Präsentieren Sie auf der **Startseite** die neusten Projekte und bieten Sie eine Übersicht aller Projektkategorien an. Besucherinnen und Besucher sollen sich zudem für einen Newsletter eintragen können, damit sie über neue Projekte informiert bleiben.

Präsentieren Sie auf der **Projektseite** alle Projekte in einer Übersicht und zeigen Sie auf einer Detailseite das jeweilige Projekt mit Bildern und Beschreibung. Bei jedem Projekt soll direkt über ein Formular eine Anfrage für einen Beratungstermin versendet werden können. Teilen Sie die Projekte in Kategorien ein. Es sollen alle Projekte oder nur die Projekte einer bestimmten Kategorie angezeigt werden können.

Bieten Sie auf einer **Kontaktseite** Informationen über das Team an und ein Kontaktformular für alle Anfragen. Über ein Dropdown im Formular sollen verschiedene Betreffzeilen ausgewählt werden können, damit das E-Mail-Programm die Anfragen später automatisch zuordnen kann.

### Anforderungen

-   100% eigener HTML- und CSS-Code (keine Frameworks)

-   Seitenstruktur mit Header, Main und Footer

-   Grid-Layout für Projektübersicht und Inhalte

-   Formulare für Terminanfragen, Newsletter und Kontakt (PHP-Mailer)

-   Responsives Design für Mobile und Desktop

### Umfang

-   Webauftritt mit mindestens 3 Hauptseiten (siehe Sitemap)

-   10 Projekte oder mehr inkl. Bilder und Beschreibung

### Ziele und Meilensteine

-   Entwurf als Wireframes für alle Seiten gemäss Sitemap

-   Erstellung einer Styleguide-Seite mit Typografie und Farben

-   Umsetzung von Struktur und Layout als Klickprototyp

-   Erfassung aller geforderten Inhalte

-   Finalisierung und Test

-   Veröffentlichung und Präsentation auf einem Webserver

### Zeit

20 Lektionen

### Optional

-   Terminbuchung mit Calendly (JavaScript erlaubt)

-   Merkliste für Projekte (Web Storage API)

### Sitemap

![Sitemap](Assets/71d2a182fc0ebf3d951f4ee40176319d.png)
