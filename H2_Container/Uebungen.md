# H2 Übungen

> Machen Sie die Übungen ohne Boilerplate, Framework oder ähnliches. Schreiben Sie 100% eigenen Code und präsentieren Sie das Ergebnis Ihrer Lehrperson. Geben Sie das Ergebnis als ZIP-Datei ab.



### Übung 1 - News-Seiten untersuchen

Gehen sie auf die Suche bei verschiedenen Tageszeitungen und testen sie, ob die HTML-Tags korrekt verwendet wurden. Sie können die Developer-Tools des Browsers verwenden, um den Code zu analysieren.



### Übung 2 - Wireframe für einen Blog

Erstellen sie Wireframes für eine Blog Website. Sie benötigen mindestes zwei Wireframes - eines für die Liste der Blog-Einträge und eines für den gesamten Blogtext, inkl. Kommentare.

Sie können draw.io oder andere Tools verwenden, aber auch auf Papier zeichnen.



### Übung 3 - HTML-Struktur für einen Blog

Erstellen sie die das HTML für ihre Wireframes aus Übung 2. Verwenden sie die korrekten Tags. Sie können ihr Wireframe auch bereits erweitern, falls sie noch zusätzliche Tags verwenden möchten.
**Achtung**: Wir erstellen das Design zu diesem Zeitpunkt noch nicht. Dies bedeutet, dass ihre HTML-Tags noch nicht wie in Übung 2 dargestellt werden.