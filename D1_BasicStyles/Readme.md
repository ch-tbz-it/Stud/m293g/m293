![TBZ-Logo](../x_gitressources/tbz_logo.png)

# CSS-Grundlagen (D1)

TBZ Informatik Modul 293 - Webauftritt erstellen und veröffentlichen

[TOC]

## Lernziele

1. Ich kann erläutern wozu ein [CSS-Styling](https://www.w3schools.com/html/html_css.asp) dient.
2. Ich kann erklären wie CSS in ein HTML-Dokument eingebunden werden kann.
3. Ich kann die wichtigsten Style-Tags für die Textformatierung nennen.
4. Ich kann die vier Bereiche des Boxmodells anwenden.
5. Ich kann ein einhetliches Styling mit CSS umsetzen.



## Einführung

### Was ist CSS?

CSS steht für [Cascading Style Sheets](https://de.wikipedia.org/wiki/Cascading_Style_Sheets) und wurde als **Gestaltungssprache** für HTML-Dokumente entwickelt. CSS ist keine Programmiersprache und auch keine Markup-Sprache, sondern beschreibt die visuelle Darstellung eines HTML-Elements im Browser in Form von Regeln. Grundidee ist die Trennung von Information und Darstellung.

Geht es um die Gestaltung einer Webseite, spricht man von [Webdesign](https://de.wikipedia.org/wiki/Webdesign). Es gibt die **strukturelle**, die **funktioninale** und die **visuelle** Gestaltung. Während HTML vorwiegend die Struktur und den Inhalt beschreibt, beschreibt CSS das Design.

CSS basiert auf einer standardisierten [W3C-Spezifikation](https://www.w3.org/Style/CSS/). Als aktuelle Version gilt CSS3 und wird von allen wichtigen Browsern (Edge, Firefox, Chrome, Safari, Opera) unterstützt.

[CSS-Beispiel testen](https://www.w3schools.com/css/demo_default.htm)

### Wie funktioniert CSS?

CSS-Code besteht aus einfachen Regeln, die immer gleich aufgebaut sind. Eine Regel beginnt mit einem Selektor, gefolgt von einer Deklaration die für eine CSS-Eigenschaft einen Wert festlegt. Sind mehrere Eigenschaften deklariert, werden sie der Reihe nach angewendet.

![CSS-Regel](Assets/css-regel-aufbau.svg)

[Mehr über die CSS-Syntax lernen...](https://www.w3schools.com/css/css_syntax.asp)

**Syntax**

~~~css
selektor {
    eigenschaft: wert;
    eigenschaft: wert;
}
~~~

**Beispiel**

~~~css
h1 {
  font-size: 72px;
  color: purple;
}
~~~

[Beispiel in Codepen testen](https://codepen.io/tbz-m293/pen/zYPqbwg?editors=1100)

### Wie wird CSS eingebunden?

Es bestehen grundsätzlich 3 Möglichkeiten CSS-Code in einem HTML-Dokument hinzuzufügen. Für eine gute Wartbarkeit, sollte der CSS-Code vom HTML-Code möglichst getrennt bleiben. Da eine CSS-Eigenschaft überschrieben werden kann, ist es wichtig die Reihenfolge zu kennen, in der CSS-Code im Browser ausgeführt wird:

**Inline CSS**

Der CSS-Style wird im [Style-Attribut](https://www.w3schools.com/html/html_styles.asp) eines HTML-Elements beschrieben. Inline CSS hat die höchste Priorität und überschreibt im Browser andere Deklarationen.

~~~html
<h1 style="color:orange;">Ich bin ein Titel</h1>
~~~

**Embedded CSS**

Der CSS-Style wird als Code-Block innerhalb eines [Style-Tags](https://www.w3schools.com/tags/tag_style.asp) beschrieben. Embedded CSS wird oft in der frühen Webentwicklung angewendet und hat im Browser zweite Priorität.

~~~html
<style type="text/css">
    h1 {
        color: orange;
    }
</style>
~~~

**External CSS**

Der CSS-Style wird in einer eigenen CSS-Datei beschrieben und im HTML-Head verlinkt.

~~~html
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
~~~

~~~css
/* style.css */
h1 {
    color: orange;
}
~~~



## Selektoren

Mit Selektoren werden alle HTML-Elemente ausgewählt, auf die eine CSS-Regel angewendet werden soll. Mit einem Selektor kann eine ganze Reihe von Elementen ausgewählt werden. Es gilt zuerst die Grundregeln zu deklarieren, die ein einheitliches Styling sicherstellen sollen. Abweichungen und Ausnahmen werden durch zusätzliche Regeln deklariert.

CSS bietet verschiedene Möglichkeiten HTML-Elemente zu selektieren. Hier die wichtigsten Selektoren:

### Typenselektor

Dieser Selektor wählt alle HTML-Elemente aus, die den angegebenen `elementname` matchen. Beispiel: `input {}` matched jedes `<input>` Element.

### Klassenselektor

Dieser Selektor wählt HTML-Elemente anhand ihres `class` Attributs aus. Eine Klasse kann mehrfach in einem Dokument verwendet werden. Beispiel: `.hide {}` matched jedes Element mit `class="hide"` als Attribut.

### ID-Selektor

Dieser Selektor wählt HTML-Elemente anhand ihres `id` Attributs aus. Es sollte nur ein Element mit der angegebenen ID in einem Dokument geben. Beispiel: `#cart` matched das Element mit `id="cart"` als Attribut.

[Mehr über CSS-Selektoren lernen](https://www.w3schools.com/css/css_selectors.asp)



## Elemente formatieren

### Fonts

Eine Schriftart definiert die grafische Gestaltung einer Satzschrift. Auch im Browser können Texte in unterschiedlichen Schriftarten dargestellt werden. Dies ist jedoch erst mit CSS3 in vollem Umfang möglich. Zuvor war die Deklaration auf interne "websichere" Schriften wie Times New Roman, Verdana oder Arial beschränkt.

Seit der Spezifizierung des [Web Open Font Format (WOFF)](https://de.wikipedia.org/wiki/Web_Open_Font_Format) und CSS3 können auch externe CSS-Schriften im Browser geladen werden. Eine der grössten freien [Schriftbibliotheken für Webfonts](https://fonts.google.com) stellt Google zur Verfügung.

Für die Deklaration von Schriften stehen umfangreiche Eigenschaften zur Verfügung. Zu den wichtigsten Eigenschaften zählen die `font-family`, die `font-weight` und die `font-size`. Sie beschreiben welche Schriftart verwendet und wie schwer und wie gross diese dargestellt werden soll.

[Mehr über CSS-Font lernen...](https://www.w3schools.com/css/css_font.asp)

**Beispiel**

~~~css
h1 {
    font-family: Helvetica, sans-serif;
    font-weight: bold;
    font-style: italic;
    font-size: 72px;
}
~~~

### Text

Für die Formatierung von Text stehen im Browser umfangreiche CSS-Eigenschaften für Farbe, Ausrichtung, Dekoration, Transformation und Abstände zur Verfügung. Wird eine Eigenschaft nicht deklariert, wird die Darstellung durch den Standard-Stil des Browsers bestimmt oder vom Elternelement geerbt. Das folgende Beispiel zeigt einige der wichtigsten Deklarationen zur Textformatierung.

[Mehr über CSS-Text lernen...](https://www.w3schools.com/css/css_text.asp)

**Beispiel**

~~~css
h1 {
    color: red;
    text-align: center;
    text-decoration: underline;
}

p {
    line-height: 150%;
}

span {
    text-transform: uppercase;
}
~~~

[Beispiel in Codepen testen](https://codepen.io/tbz-m293/pen/Exbyzdp?editors=1100)

### Listen

Listen und Aufzählungen werden in HTML oft als Strukturelemente verwendet und auch für Menus eingesetzt. CSS bietet verschiedene Listenstile und Aufzählungszeichen. Diese werden in Menus jedoch oft gar nicht gebraucht und mit CSS entsprechend entfernt.

[Mehr über CSS-Listen lernen...](https://www.w3schools.com/css/css_list.asp)

**Beispiel**

~~~css
ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
}
~~~

[Beispiel in Codepen testen](https://codepen.io/tbz-m293/pen/dyZzQQZ?editors=1100)

### Links

Ein Link zu einer anderen Webseite oder zu einem Anker im selben Dokument können wie Text formatiert werden. Das besondere ist jedoch, dass ein Link verschiedene Zustände besitzt, die über [Pseudoklassen](https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-classes) selektiert werden können.

[Mehr über CSS-Links lernen...](https://www.w3schools.com/css/css_link.asp)

**Beispiel**

~~~css
/* Link allgemein */
a { color: blue; }

/* States einzeln */
a:link      { color: red; }
a:visited   { color: green; }
a:hover     { color: hotpink; }
a:active    { color: orange; }
~~~



## Boxmodell anwenden

In CSS ist jedes HTML-Element von einer Box umgeben. Deshalb ist es absolut zentral, das [Boxmodell](https://en.wikipedia.org/wiki/CSS_box_model) in all seinen Eigenschaften zu verstehen. Es dient dazu den Textfluss zu steuern und sogar ganze Layouts für Webseiten zu erstellen.

![Boxmodell-Grafik](Assets/boxmodell-detail-wm.png)

Das Boxmodell besteht aus vier Bereichen und beschreibt den Abstand zu jeder Seite nach innen und nach aussen. Im innersten befindet sich der `content`, danach folgt das `padding`, danach der `border` und ganz aussen die `margin`. Die Schwierigkeit besteht darin, die Gesamtgrösse einer Box zu berechnen. Je nach Deklaration der Eigenschaft `box-sizing` wird anders gerechnet.

[Mehr über das CSS-Boxmodell lernen...](https://www.w3schools.com/css/css_boxmodel.asp)

### Box-Sizing

Die Eigenschaft `box-sizing` wurde erst mit CSS3 eingeführt. Bis zu diesem Zeitpunkt wurde die Gesamtgrösse einer Box immer als `content-box` berechnet. Dabei werden alle Abstände zur Breite und Höhe des Contents addiert, was zu verschiedenen Problemen insbesondere mit Prozent-Werten führt.

In der modernen Webentwicklung wird als `box-sizing` die `border-box` verwendet. Damit zählt der `border` und das `padding` als echter Innenabstand und hat keinen Einfluss auf die Breite oder Höhe eines Elements, was die Berechnung stark vereinfacht. **Wichtig:** Der Browser-Standard für `box-sizing` ist historisch bedingt `content-box` und muss daher mit `border-box` explizit deklariert werden.

~~~css
h1 {
    box-sizing: border-box;
    height: 50%;
    padding: 20px;
    border-width: 20px;
    border-style: solid;
    border-color: purple;
    margin: 20px;
    background-color: violet;
}
~~~

[Beispiel mit CodePen testen](https://codepen.io/tbz-m293/pen/abVLOPg?editors=1100)

### Content

Als Content versteht sich der eigentliche Inhalt eines HTML-Elements, oft in Form von Text oder weiteren Elementen. Im Boxmodell ist dies der **innerste Bereich**.

### Padding

Das `padding` wird auch **Innenabstand** genannt und in der `border-box` als Abstandsregel nach innen verwendet. Abstände nach innen sind besonders in Layouts wichtig, um den Weissraum rund um Text zu beschreiben. Weissraum ist ein wichtiger Bestandteil von Typografieregeln, die ein einhheitliches Erscheinungsbild schaffen.

[Mehr über CSS-Padding lernen...](https://www.w3schools.com/css/css_padding.asp)

### Border

Der `border` ist der Rahmen zwischen `padding` und `margin` und hat eine visuelle Funktion. Er verfügt über verschiedene Eigenschaften zur Darstellung der Rahmenkontur wie Breite, Stil oder Farbe. Die am meisten genutzte Eigenschaft ist jedoch der `border-radius` mit dem die Ecken einer Box abgerundet dargestellt werden können.

[Mehr über CSS-Border lernen...](https://www.w3schools.com/css/css_border.asp)

### Margin

Die `margin` beschreibt den **Aussenabstand** einer `border-box`. Sie ist die Abstandsregel nach Aussen zu anderen HTML-Elementen. Abstände nach aussen finden besonders in Listen und Grids eine häufige Verwendung. Grids sind ein zentraler Kern von Layouts. Dank ihnen können Elemente an einem Raster ausgerichtet werden.

[Mehr über CSS-Margin lernen...](https://www.w3schools.com/css/css_margin.asp)



## Anzeige steuern

### Display

Der Textfluss eines HTML-Elements wird durch die umschliessende Box bestimmt. Die `display` Eigenschaft legt die Darstellung dieser Box fest und unterscheidet primär zwischen `inline` und `block`. Block-Elemente wie Überschirften oder Absätze nehmen die gesamte Breite ein und brechen überstehenden Text um. Der Wert `none` erlaubt es ein Element gänzlich auszublenden.

[Mehr über CSS-Display lernen...](https://www.w3schools.com/css/css_display_visibility.asp)

**Beispiel**

~~~css
h1 {
    display: block; /* default*/
    background-color: lightblue;
}

span {
    display: inline; /* default */
    background-color: lightgreen;
    padding: 1em;
    margin: 1em;
}

button {
    display: none;
}
~~~

[Beispiel in Codepen testen](https://codepen.io/tbz-m293/pen/ExbvBwP?editors=1100)

### Visibility
Die CSS-Eigenschaft `visibility` steuert die Sichtbarkeit eines Elements. Mit dem Wert `hidden` kann ein Element unsichtbar gemacht werden, mit `visible` wieder Sichtbar. Das Element braucht jedoch immer noch Platz und wird in der **Grössenberechnung weiter berücksichtigt**. Es gibt nur wenige sinnvolle Anwendungen dafür. Verwenden Sie im Zweifelsfall immer `display: none;`, um das Element aus der Anzeige zu entfernen, anstatt es nur unsichtbar zu machen.

### Dimensionen

Über die CSS-Eigenschaft `width` und `height` kann die Breite und Höhe eines Block-Elements bestimmt werden. Wird der Wert in Prozent deklariert, versteht sich die Angabe immer realtiv zum maximalen Viewport des Browsers. Neben festen Eigenschaften bietet CSS auch dynamische Eigenschaften wie `max-width` oder `min-height` die für [Layouts mit CSS] interessant sind.

[Mehr über CSS-Dimensionen lernen...](https://www.w3schools.com/css/css_dimension.asp)

**Beispiel**

~~~css
TODO: CSS Beispiel
~~~



## Farben

### Farbdefinitionen

In CSS können für fast alle HTML-Elemente Farben deklariert werden. Es sind jedoch verschiedene Formen zur Definition eines Farbwerts möglich. CSS3 bringt [140 Farbnahmen](https://htmlcolorcodes.com/color-names/) mit, die in modernen Browsern unterstützt werden. In der Webentwicklng werden jedoch oft die [hexidezimale Farbdefinition](https://de.wikipedia.org/wiki/Hexadezimale_Farbdefinition) verwendet. Der RGB-Wert einer Farbe wird in [HEX](https://www.w3schools.com/css/css_colors_hex.asp) mit nur 6 oder 3 Zeichen definiert und ist damit sehr kompakt. Weitere Schreibweisen sind [RGB](https://www.w3schools.com/css/css_colors_rgb.asp) und [HSL](https://www.w3schools.com/css/css_colors_hsl.asp), die sich primär in ihrem Farbsystem unterscheiden. Beide arbeiten mit dezimalen Farbwerten und können um einen Wert für die Transparenz erweitert werden.

[Mehr über CSS-Farben lernen...](https://www.w3schools.com/css/css_colors.asp)

**Farbnamen**

~~~css
h1 {
    color: orange;
    border: 10px solid gold;
    background-color: ivory;
    padding: 1em;
}
~~~

**HEX-Definition**

~~~css
h1 {
    color: #FFA500;
    border: 10px solid #FFD700;
    background-color: #FFFFF0;
    padding: 1em;
}
~~~

**RGB-Definition**

~~~css
h1 {
    color: rgb(255, 165, 0);
    border: 10px solid rgb(255, 215, 0);
    background-color: rgba(255, 255, 240, 0.5); /* Mit 50% Transparenz */
    padding: 1em;
}
~~~

**HSL-Definition**

~~~css
h1 {
    color: hsl(39, 100%, 50%);
    border: 10px solid hsl(51, 100%, 50%);
    background-color: hsl(60, 100%, 97%, 0.5); /* Mit 50% Transparenz */
    padding: 1em;
}
~~~

[Beispiele mit CodePen testen](https://codepen.io/tbz-m293/pen/rNYGvGj?editors=1100)

### Farbtools

Im Webdesign besteht oft der Bedarf ein festes Set an Farben zu finden, die gut zusammen passen. Dabei spricht man von einem Farbschema. Im Web finden sich verschiedene Tools, die bei der Findung eines individuellen Farbschemas und der Umrechnung von Farbwerten helfen. Die folgenden drei Tools bieten einen guten Funktionsumfang und ermöglichen auch das Speichern von eigenen Farbschemas:

1. [Coolors](https://coolors.co)
2. [Adobe Color](https://color.adobe.com)
3. [Color Hunt](https://colorhunt.co)



## Masseinheiten

CSS bietet eine Reihe unterschiedlichen Masseinheiten. Einige wie `pt` **(Punkt)** oder `pc` **(Pica)** haben ihre Wurzeln in der Typografie und andere wie `in` **(Inch)**, `cm` **(Zentimeter)** oder `mm` **(Millimeter)** kennen wir aus unserem Alltag.

Masseinheiten sind auf keine Eigenschaften beschränkt und können für jede beliebige Länge verwendet werden. Sie sollten jedoch mediengerecht eingesetzt werden. In der modernen Webtypografie wird praktisch nur noch `px` **(Pixel)** und `em` verwendet.

[Mehr über CSS-Einheiten lernen...](https://www.w3schools.com/cssref/css_units.asp)

### Absolute Grössen

`px` **(Pixel)** ist die meistgenutzte Einheit in CSS und hat sich als Standard etabliert. Ein Pixel ist kein echter Bildpunkt, sondern eine absolute Referenzgrösse und entspricht 1/96 Inch (Zoll). Displaygrösse, Pixeldichte, Desktop oder Mobile spielt dabei keine Rolle. In CSS ist ein Pixel eine virtuelle Grösse und stimmt nur selten mit einem Bildschirmpixel überein.

`pt` **(Point)** kommt aus der Typografie und entspricht 1/72 Inch. Mit `pc` **(Pica)** steht noch eine grössere Einheit mit Faktor 12 zur Verfügung, was 1/6 Inch entspricht. Keine der beiden wird heute noch eingesetzt, ausser vielleicht für Druckansichten.

`in` **(Inch)** entspricht 2.54cm und ist eine angloamerikanische Masseinheit. In CSS wird sie kaum verwendet, ausser es gilt z.B. ein Druckerzeugnis zu beschreiben. Für einen US Letter im Hochformat mit 8 1/2 x 11 Inch wäre es einfacher die Längenangaben in Inch zu deklarieren.

`mm` **(Millimeter)** ist eine metrische Masseinheit, die wie `cm` **Zentimeter** in CSS kaum verwendet wird. Für eine Druckansicht als A4 im Hochformat mit 210 x 297 mm wäre es sicher sinnvoll und mediengerecht eine dieser Masseinheiten zu benutzen.

### Relative Grössen

`em` bescheibt das Vielfache der Schrifthöhe des aktuellen HTML-Elements. Sie ist eine der wichtigsten relativen Masseineheiten. Mit `line-height: 1.5em;` ist z.B. die Linienhöhe immer das 1.5-fache der Schriftgrösse, egal wie diese verändert wird. `em` wird auch im Boxmodell für Abstände genutzt. Responsive Design lässt grüssen. Weil die Grösse relativ zur Schrifthöhe steht, werden alle Abstände in `em` entsprechend mit skaliert, wenn sich die Schrifthöhe des Elements ändert.

`rem` bezieht sich immer auf das Root-Element `html` und beschreibt genau wie `em` das Vielfache der Schrifthöhe. Als Browser-Standard gilt für das Root-Element eine Schriftgrösse von `16px`. Die Grösse `font-size: 3rem;` ist also eine um Faktor 3 grössere Schrift mit `48px` relativ zum Root-Element und skaliert bei einer Änderung mit. Dies ist einer der häuffigsten Anwendungsfälle und erlaubt es für ein Design [Typografieregeln] aufzustellen.

`%` **(Prozent)** ist die meist verbreitete relative Grösse für Aussenmasse. Sie beschreibt die Breite oder Höhe eines Block-Elements in relation zur Dimension der umschliessenden Box in Prozent. Leider ist die Berechnung von der Richtung der Eigenschaft abhängig, die deklariert wird und schränkt uns bei der Anwendung etwas ein. So bezieht sich `width: 50%;` immer auf die Breite der umschliessenden Box und kann sich nie an der Höhe orientieren.

`vw` **(View-Width)** und `vh` **(View-Height)** gehören zu den neusten Masseinheiten in CSS und entsprechen einem 1/100stel der Breite oder der Höhe. Ähnlich wie `%` berechnet sich die Grösse relativ, jedoch auf den Viewport bezogen. Ein weiterer Vorteil ist, dass beide Masseinheiten unabhängig der Richtung für alle Eigenschaften verwendet können und sich speziell für Grids und [Layouts] eignen.



## Typografieregeln

### Zeilenhöhe

Um ein einheitliches Schriftbild zu erreichen helfen ein paar einfache Regeln. Grundsätzlich fehlt es bei Text fast immer an Weissraum. Textzeilen und Abstände wirken zu eng. Deshalb sollte für Text immer eine Zeilenhöhe definiert werden, die das Lesen von Text erleichtert.

**Beispiel**

~~~css {
html {
    line-height: 1.5em;
}
~~~

### Überschriften

Headline, Titel und Untertitel sollten immer in einem festen Verhältnis zu einander stehen, damit die Hierarchie deutlich erkannt wird. Grosse Headlines werden oft als Stilmittel eingesetzt. Als Faustregel können Faktoren von 1.25, 1.5 und 2 angewendet werden.

**Beispiel**

~~~css
h1 {
  font-size: 6rem;
}

h2 {
  font-size: 3rem;
}

h3 {
  font-size: 1.5rem;
}
~~~

### Abstände

Damit Text genug Raum erhält und vom Auge gut erfasst werden kann, sollte in der umschliessenden Border-Box genug Innenabstand definiert werden. Hier empfiehlt sich ebenfalls in Faktoren zu Rechnen. Ideal ist wenn der Innenabstand immer in Relation zur Schrifthöhe des Elements steht. Dazu kann das `padding` einfach in `em` deklariert werden.

**Beispiel**

~~~css
div {
    padding: 1.5em;
}
~~~

[Alle Beispiele mit CodePen testen...](https://codepen.io/tbz-m293/pen/PoOOZGy?editors=1100)



## Styleguide

### Corporate Design

Corporate Design (kurz **CD**) bezeichnet ein bewusst definiertes, einheitliches Erscheinungsbild eines Unternehmens oder einer Marke. Dazu gehören grafisch abgestimmte Produktverpackungen und Geschäftsausstattung, aber auch optisch passende Marketingmassnahmen. Ein Markenlogo und ein Farbkonzept sind ebenfalls elementarer Bestandteil eines Corporate Designs.

Übergeordnet steht die Corporate Identity (kurz **CI**), zu der – neben dem Corporate Design – auch der Kommunikationsstil, öffentliche Verhaltensweisen, Unternehmensphilosophie und einiges mehr zählen.

### Branding

In modernen Unternehmen ist heute die Marke (engl. **Brand**) das Zentrum des Designs. Kurz gesagt wird versucht die Marke emotional aufzuladen. Die Marke ist oft das Wertvollste eines Unternehmens und jede Erfahrung mit ihr soll eine positive Erfahrung sein. Dahinter steckt die Disziplin des Brand Designs. Deshalb werden in [Brand Guidelines] auch die wichtigsten Design-Regeln festgehalten:

1. Anwendung des **Logos** in allen Grössen und Medien
2. Farbpalette mit allen **Farben** und Kombinationen
3. Typografie mit **Schriftarten**, Schriftgrössen und Abständen
4. Bildwelt mit **Fotos**, Illustrationen und Grafiken

Wie schnell zu erkennen ist, handelt es sich dabei um Elemente eines [Styleguides]. CSS ist im Prinzip nicht anderes als ein Styleguide, bestehend aus Regeln in Form von Code. Ein Styleguide stell sicher, dass ein Design nach den immer gleichen Regeln aufgebaut ist und damit ein **einheitliches und positives Erscheinungsbild** erreicht werden kann.

[Beispiel eines Styleguides ansehen](https://discord.com/branding)



## Gewichtung in CSS verstehen

### Kaskade und Vererbung

Ein besonderes Merkmal von CSS ist die Kaskade (**Cascading** Style Sheets). Ein Dokument wird oft durch mehrere Stylesheets formatiert, die aus verschiedenen Quellen stammen und aufeinander aufbauen. Wird eine Regel oder Eigenschaft mehrfach deklariert, wird die zuletzt deklarierte Eigenschaft verwendet.

Ein weiteres Merkmal von CSS ist die Vererbung von Eigenschaften. Kindelemente können Eigenschaften ihrer Elternelemente erben. Die Vererbung wird über den Wert `inherit` gesteuert, funktioniert jedoch nicht mit allen Eigenschaften.

[Mehr über CSS-Kaskade und CSS-Vererbung lernen...](https://developer.mozilla.org/docs/Learn/CSS/Building_blocks/Cascade_and_inheritance)

**Beispiel**

~~~css
h1, h2, h3 {
    font-weight: normal;
    color: red;
}

h1 {
    color: orange;
    color: green;
}

strong {
    font-weight: inherit;
}
~~~

[Beispiel in Codepen testen](https://codepen.io/tbz-m293/pen/RwjgzYB?editors=1100)

### Spezifität

Mit CSS können HTML-Elemente sehr spezifisch selektiert werden. Je spezifischer ein HTML-Element selektiert wird, desto höher ist die Gewichtung einer CSS-Regel. Im folgenden Beispiel wird `h1.headline` höher gewichtet als `h1` oder `.headline`, egal in welcher Reihenfolge dies deklariert wurde. 

[Mehr über die CSS-Spezifität lernen...](https://www.w3schools.com/css/css_specificity.asp)

**Beispiel**

~~~css
h1.headline {
    color: orange;
}

.headline {
    font-size: 72px;
    color: red;
}

h1 {
    font-family: Helvetica, sans-serif;
    color: green;
}
~~~

[Beispiel in Codepen testen](https://codepen.io/tbz-m293/pen/GROEeVz?editors=1100)



## Checkpoints

- [ ] Ich kann erläutern wozu ein [CSS-Styling](https://www.w3schools.com/html/html_css.asp) dient.
- [ ] Ich kann erklären wie CSS in ein HTML-Dokument eingebunden werden kann.
- [ ] Ich kann die wichtigsten Style-Tags für die Textformatierung nennen.
- [ ] Ich kann die vier Bereiche des Boxmodells anwenden.
- [ ] Ich kann ein einhetliches Styling mit CSS umsetzen.



## Ressourcen

1. [Cascading Style Sheets](https://de.wikipedia.org/wiki/Cascading_Style_Sheets) (Wikipedia)
2. [CSS-Tutorial](https://www.w3schools.com/css/default.asp) (W3Schools)
3. [CSS first steps](https://developer.mozilla.org/de/docs/Web/CSS) (Mozilla Developer Web Docs)
4. [CSS lernen und anwenden](https://www.w3.org/Style/CSS/learning) (W3C Cascading Style Sheets)
5. [CSS Einstieg und Grundlagen](https://www.mediaevent.de/css/) (Mediaevent CSS mit Stil)



**Informatik Modul 293 - Webauftritt erstellen und veröffentlichen**

Letzte Aktualisierung: 17.05.2022, Florian Huber